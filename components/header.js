import React from 'react'
import {View, StyleSheet, Text} from 'react-native'

export default function Header() {
    return(
        <View style={styles.header}>
            <Text style={styles.title}>My Todo List</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    header:{
        backgroundColor:'coral',
        height:60,
        marginTop:'6%',
        textAlign:'center',
        paddingTop:15,
        alignItems:'center',
    },
    title:{
        fontWeight:'bold',
        fontSize:20,
    },
});