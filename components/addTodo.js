import React, { useState } from 'react';
import { TextInput, StyleSheet, Button, View } from 'react-native';

export default function AddTodo({ submitHandler }) {
    const [text, setText] = useState('');

    return(
        <View>
            <TextInput
                style={styles.input}
                placeholder= 'New subject...'
                onChangeText= {(val) => {setText(val)}}
            />
            <Button title='Add new' color='coral' onPress={() => {submitHandler(text)}}/>
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        padding:10,
        borderBottomWidth:1, 
        fontSize:20,
        marginVertical:20,
    },
    
});