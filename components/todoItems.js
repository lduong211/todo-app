import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

export default function TodoItem({ item, pressHandler }) {
    return(
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <Text style={styles.list}>{item.text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    list: {
        borderRadius:20,
        borderWidth:1,
        textAlign:'center',
        marginVertical: 20,
        padding:20,
        borderStyle:'dashed',
    },
})