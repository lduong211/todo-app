import React, { useState } from 'react';
import { Alert, FlatList, Keyboard, ScrollView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import AddTodo from './components/addTodo';
import Header from './components/header';
import TodoItem from './components/todoItems';


export default function App() {

  const [todos, setTodos] = useState([
    {text: 'Task 1', key: '1'},
    {text: 'Task 2', key: '2'},
    {text: 'Task 3', key: '3'},
    {text: 'Task 4', key: '4'},
  ]);

  const pressHandler = (key) => {
    setTodos((prevTodos) => {
      return prevTodos.filter(todo => todo.key != key);
    });
  }

  const submitHandler = (text) => {

    if(text.length > 3){
      setTodos((prevTodos) => {
        return([
          ...prevTodos,
          {text: text, key: Math.random().toString() },
        ])
      });
    } else {
        Alert.alert('Can not submit', 'Subject must be 3 chars long',[{text:'Understood', onPress: () => console.log('alert closed')}]);
    }
    
  }

  // const submitHandler = (text) => {
  //   setTodos((prevTodos) => 
  //     [{text: text, key: Math.random().toString()}]
  //   )
  // }

  return (
    <TouchableWithoutFeedback onPress={() => {
      Keyboard.dismiss();
    }}>
    <View style={styles.container}>
      <Header></Header>
      <View style={styles.content}>
        <AddTodo submitHandler={submitHandler}></AddTodo>
        <FlatList
          data = {todos}
          renderItem = {({ item }) => (
            <TodoItem item={item} pressHandler={pressHandler}></TodoItem>
          )}
        />
      </View>
    </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    paddingHorizontal:30,
    paddingVertical:20,
  }
  
});
